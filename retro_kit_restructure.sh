for FILENAME in ./*; do

    FILENAME_REGION_FIRST_PARENTHESIS_REMOVED=$(echo ${FILENAME/*"("/})
    FILENAME_REGION_LAST_PARENTHESIS_REMOVED=$(echo ${FILENAME_REGION_FIRST_PARENTHESIS_REMOVED/")"*/})
    FILENAME_REGION=${FILENAME_REGION_LAST_PARENTHESIS_REMOVED}

    STRIPPED_FILENAME=$(echo ${FILENAME} | cut -f1 -d"(")
    DIRECTORY_NAME=${STRIPPED_FILENAME}

    # If directory doesn't exist, make it
    if [ ! -d "${DIRECTORY_NAME}" ]; then
        mkdir "${DIRECTORY_NAME}"
    fi

    FILENAME_REGION_PATH="${DIRECTORY_NAME}/${FILENAME_REGION}"
    if [ ! -d "${FILENAME_REGION_PATH}" ]; then
        mkdir "${FILENAME_REGION_PATH}"
        mkdir "${FILENAME_REGION_PATH}/Instruction Manual"
        mkdir "${FILENAME_REGION_PATH}/Box"
        mkdir "${FILENAME_REGION_PATH}/Inserts"
    fi

    mv "${FILENAME}" "${FILENAME_REGION_PATH}/Instruction Manual"
done
